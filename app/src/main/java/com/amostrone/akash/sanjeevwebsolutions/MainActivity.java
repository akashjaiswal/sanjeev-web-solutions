package com.amostrone.akash.sanjeevwebsolutions;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.AlertDialog;


import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private WebView mywebView;
    private ProgressBar bar;
    private ScrollView scroll_about;
    private ScrollView scroll_policy;
    private RelativeLayout rel_contact;

    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;

    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseAuth mAuth;

    private GoogleSignInClient mGoogleSignInClient;

    private String web_host = "http://www.sarkariresulta.com";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Obtain the FirebaseAnalytics and authentication instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mAuth = FirebaseAuth.getInstance();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        scroll_about = findViewById(R.id.content_frame_about);
        scroll_policy = findViewById(R.id.content_frame_policy);
        rel_contact = findViewById(R.id.content_frame_contact);

        bar= findViewById(R.id.bar);
        bar.setVisibility(View.INVISIBLE);


        // STATUS BAR COLOR. BY DEFAULT IT DOES NOT WORK IN LYF
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().hide();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mywebView = findViewById(R.id.webview);
        WebSettings webSettings = mywebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mywebView.loadUrl(web_host);
        mywebView.setWebViewClient(new WebViewClient());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        switch (id) {
            case R.id.action_refresh:
                String url = mywebView.getUrl();
                mywebView.loadUrl(url);
                break;
            case R.id.action_share_link:
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "My application name");
                    String sAux = mywebView.getUrl();
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "Choose one"));
                } catch(Exception e) {
                    //e.toString();
                }
                break;
            /*case R.id.action_sign_in:
                signIn();
                break;
            case R.id.action_sign_out:
                signOut();
                break;*/
            case R.id.action_rateus:
                intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com"));
                startActivity(intent);
                break;
            case R.id.action_desktop:
                mywebView.getSettings().setUserAgentString("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
                break;
            /*case R.id.facebook:
                intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.facebook.com/SanjeevWebSolutionsOpcPvtLtd/"));
                startActivity(intent);
                break;
            case R.id.twitter:
                intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://twitter.com/SanjeevTelecom"));
                startActivity(intent);
                break;
            case R.id.googleplus:
                intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://plus.google.com/u/1/116993907797837041793"));
                startActivity(intent);
            break;*/
            //case R.id.action_revoke:
            //revokeAccess();
            //break;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        //TODO Change both layout and main layout

        switch (id) {
            case R.id.home:
                mywebView.setVisibility(View.VISIBLE);
                mywebView.loadUrl(web_host);
                bar.setVisibility(View.INVISIBLE);
                scroll_policy.setVisibility(View.INVISIBLE);
                rel_contact.setVisibility(View.INVISIBLE);
                scroll_about.setVisibility(View.INVISIBLE);
                break;

            case R.id.contact_us:
                mywebView.setVisibility(View.INVISIBLE);
                bar.setVisibility(View.INVISIBLE);
                scroll_policy.setVisibility(View.INVISIBLE);
                scroll_about.setVisibility(View.INVISIBLE);
                rel_contact.setVisibility(View.VISIBLE);
                break;

            case R.id.about_us:
                mywebView.setVisibility(View.INVISIBLE);
                bar.setVisibility(View.INVISIBLE);
                rel_contact.setVisibility(View.INVISIBLE);
                scroll_about.setVisibility(View.VISIBLE);
                scroll_policy.setVisibility(View.INVISIBLE);
                break;

            case R.id.policy:
                mywebView.setVisibility(View.INVISIBLE);
                bar.setVisibility(View.INVISIBLE);
                rel_contact.setVisibility(View.INVISIBLE);
                scroll_about.setVisibility(View.INVISIBLE);
                scroll_policy.setVisibility(View.VISIBLE);
                break;


            case R.id.exit:
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(this);

                builder.setTitle("EXIT")
                        .setMessage("Do you want to exit?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                System.exit(0);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
                break;
            case R.id.SarkariResultAll:
                mywebView.loadUrl(web_host+"/sarkari-result/");
                break;
            /*case R.id.LatestResult:
                mywebView.loadUrl(web_host+"/result-all/");
                break;
            case R.id.AllUniversity:
                mywebView.loadUrl(web_host+"/all-university/");
                break;
            case R.id.ImportantOnlineForm:
                mywebView.loadUrl(web_host+"/important/");
                break;*/
            case R.id.admit_card:
                mywebView.loadUrl(web_host+"/admit-card-all/");
                break;
            case R.id.syllabus:
                mywebView.loadUrl(web_host+"/syllabus-all/");
                break;
            case R.id.answer_key:
                mywebView.loadUrl(web_host+"/answer-key-all/");
                break;
            case R.id.admission:
                mywebView.loadUrl(web_host+"/admission-all/");
                break;
            /*case R.id.aadhar_card:
                mywebView.loadUrl(web_host+"/aadhaar-card/");
                break;
            case R.id.scholar_ship:
                mywebView.loadUrl(web_host+"/scholarship-uttar-pradesh-1001/");
                break;
            case R.id.pan:
                mywebView.loadUrl(web_host+"/apply-online-pan-card/");
                break;
            case R.id.voter:
                mywebView.loadUrl(web_host+"/votar-card-apply-online/");
                break;
            case R.id.driving:
                mywebView.loadUrl(web_host+"/driving-license-apply-online/");
                break;*/
            }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        //GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        //updateUI(account);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        //if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            //Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            //handleSignInResult(task);
        //}
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    private void signIn() {
        /*Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
        Toast toast = Toast.makeText(getApplicationContext(),
                "Signed in successfully", Toast.LENGTH_SHORT);
        toast.show();*/
    }

    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Signed out successfully", Toast.LENGTH_SHORT);
                        toast.show();
                        // [START_EXCLUDE]
                        updateUI(null);
                        // [END_EXCLUDE]
                    }
                });
    }

    private void revokeAccess() {
        mGoogleSignInClient.revokeAccess()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // [START_EXCLUDE]
                        updateUI(null);
                        // [END_EXCLUDE]
                    }
                });
    }

    private void updateUI(@Nullable GoogleSignInAccount account) {

        TextView curr_usr = findViewById(R.id.user_current);
        //ImageView profile_photo = findViewById(R.id.profile_photo_view);
        //Uri personPhoto = null;
        String name =  "Welcome to Sarkari Result All";

        if (account != null) {
            name = account.getDisplayName();
            //personPhoto = account.getPhotoUrl();
        }
        try {
            curr_usr.setText(name);
            //if(personPhoto != null) {
                //profile_photo.setImageURI(null);
               // profile_photo.setImageURI(personPhoto);
            //}
        }
        catch (Exception e)
        {
            Log.w(TAG, "signInResult:failed");
            Bundle bundle = new Bundle();
            bundle.putString("Sign_In", "google sign in crash");
            mFirebaseAnalytics.logEvent("some_name", bundle);
            //Toast toast = Toast.makeText(getApplicationContext(),
                    //"Error in Account Action", Toast.LENGTH_SHORT);
            //toast.show();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (mywebView.canGoBack()) {
            mywebView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void com_phone(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:9648894794"));
        startActivity(intent);
    }
    public void com_mail(View view) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL  , new String[]{"support@sarkariresulta.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Sanjeev Web Solutions App");
        try {
            startActivity(Intent.createChooser(intent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }
    public void dev_phone(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:9670664115"));
        startActivity(intent);
    }
    public void dev_mail(View view) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL  , new String[]{"akashjaiswal3846@gmail.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Sanjeev Web Solutions App");
        try {
            startActivity(Intent.createChooser(intent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }
    public void dev_mail_san(View view) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL  , new String[]{"info@sanjeevwebsolutions.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Sanjeev Web Solutions App");
        try {
            startActivity(Intent.createChooser(intent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }


    public class WebViewClient extends android.webkit.WebViewClient {

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

            if (errorCode == -2)
                Toast.makeText(MainActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();

            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {

            bar.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {

            bar.setVisibility(View.INVISIBLE);
            super.onPageFinished(view, url);
        }

        /*@Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (Uri.parse(url).equals(web_host)) {
                // This is my web site, so do not override; let my WebView load the page
                return false;
            }
            // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Opening external link in browser", Toast.LENGTH_SHORT);
            toast.show();
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }*/
    }
}